const findOneUsers = require("../../functions/users/findOneUsers");
const returnToUser = require("../../services/returnToUser");
const {
  generateAccessToken,
  generateRefreshToken
} = require("../../services/jwt/sign");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const _ = require("lodash");

const login = async (req, res) => {
  try {
    let { username, password } = req.body;
    let { success, payload, message } = await findOneUsers({ username });
    if (success) {
      if (await bcrypt.compare(password, payload.password)) {
        let tokenData = _.pick(payload, ["_id"]);
        if (_.isEqual(payload.role, "admin")) {
          tokenData = {
            ...tokenData,
            isAdmin: true
          };
          let accessToken = generateAccessToken(tokenData);
          let refreshToken = generateRefreshToken(tokenData);
          returnToUser.success(res, "token", {
            accessToken,
            refreshToken,
            isAdmin: true
          });
        } else {
          let employees = await mongoose
            .model("employees")
            .findOne({ accountId: payload._id });
          tokenData = {
            _id: employees._id,
            isAdmin: false
          };
          let accessToken = generateAccessToken(tokenData);
          let refreshToken = generateRefreshToken(tokenData);
          returnToUser.success(res, "token", {
            accessToken,
            refreshToken,
            isAdmin: false
          });
        }
      } else {
        returnToUser.errorWithMess(res, "wrong password");
      }
    } else {
      returnToUser.errorWithMess(res, message);
    }
  } catch (err) {
    console.log(err);
    returnToUser.errorWithMess(res, "server login false");
  }
};

module.exports = login;
