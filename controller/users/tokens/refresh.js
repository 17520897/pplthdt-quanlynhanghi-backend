const jwt = require("jsonwebtoken");
const { generateAccessToken } = require("../../../services/jwt/sign");
const returnToUser = require("../../../services/returnToUser");

const refreshToken = (req, res) => {
  try {
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY);
    if (!payload.refreshToken) {
      return res.status(401).json({
        message: "You are not authorized"
      });
    }

    delete payload.exp;
    delete payload.refreshToken;
    delete payload.iat;
    const accessToken = generateAccessToken(payload);

    return returnToUser.success(res, {
      accessToken
    });
  } catch (e) {
    console.log(e.stack);
    res.status(401).json({
      message: "You are not authorized"
    });
  }
};

module.exports = refreshToken;
