const updateEmployees = require("../../../functions/employees/updateEmployees");

const returnToUser = require("../../../services/returnToUser");

const updateEmployeesInfoController = async (req, res) => {
  let { _id } = req.employees;
  let { success, payload, message } = await updateEmployees(_id, req.body);
  if (success) returnToUser.success(res, "Update employees success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateEmployeesInfoController;
