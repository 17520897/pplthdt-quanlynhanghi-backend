const findOneEmployees = require("../../../functions/employees/findOneEmployees");

const returnToUser = require("../../../services/returnToUser");

const getEmployeesInfoController = async (req, res) => {
  let { _id } = req.employees;
  let { success, payload, message } = await findOneEmployees({ id: _id });
  if (success) returnToUser.success(res, "Employees info", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getEmployeesInfoController;
