const changeRooms = require("../../../functions/bills/changeRooms");

const returnToUser = require("../../../services/returnToUser");

const changeRoomsController = async (req, res) => {
  let { id } = req.params;
  let { roomId } = req.body;
  let { success, payload, message } = await changeRooms(id, roomId);
  if (success) returnToUser.success(res, "change rooms success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = changeRoomsController;
