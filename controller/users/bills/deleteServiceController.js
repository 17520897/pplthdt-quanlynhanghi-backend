const deleteService = require("../../../functions/bills/deleteService");

const returnToUser = require("../../../services/returnToUser");

const deleteServiceController = async (req, res) => {
  let { id } = req.params;
  let { serviceName } = req.body;
  let { success, payload, message } = await deleteService(id, serviceName);
  if (success) returnToUser.success(res, "delete service success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = deleteServiceController;
