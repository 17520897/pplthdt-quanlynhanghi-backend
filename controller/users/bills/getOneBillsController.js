const getOneBills = require("../../../functions/bills/getOneBills");
const returnToUser = require("../../../services/returnToUser");
const getOneBillsController = async (req, res) => {
  let { id } = req.params;
  let { time } = req.body;
  let { success, payload, message } = await getOneBills(id, time);
  if (success) returnToUser.success(res, "bill info", payload);
  else returnToUser.errorWithMess(res, message);
};
module.exports = getOneBillsController;
