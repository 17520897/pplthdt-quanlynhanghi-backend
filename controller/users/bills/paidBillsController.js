const paidBills = require("../../../functions/bills/paidBills");
const returnToUser = require("../../../services/returnToUser");

const paidBillsController = async (req, res) => {
  let { id } = req.params;
  let { success, payload, message } = await paidBills({
    billId: id,
    checkoutDate: req.body.checkoutDate
  });
  if (success) returnToUser.success(res, "paid bill success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = paidBillsController;
