const createBills = require("../../../functions/bills/createBills");
const returnToUser = require("../../../services/returnToUser");

const createBillsController = async (req, res) => {
  let { _id } = req.employees;
  req.body.employeesId = _id;
  let { success, payload, message } = await createBills(req.body);
  if (success) returnToUser.success(res, "create bill success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = createBillsController;
