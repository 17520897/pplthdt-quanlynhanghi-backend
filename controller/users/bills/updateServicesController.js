const updateServices = require("../../../functions/bills/updateServices");

const returnToUser = require("../../../services/returnToUser");

const updateServicesController = async (req, res) => {
  let { id } = req.params;
  let { services } = req.body;
  let { success, payload, message } = await updateServices(id, services);
  if (success) returnToUser.success(res, "update services success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateServicesController;
