const returnToUser = require("../../../services/returnToUser");
const findAllServices = require("../../../functions/services/findAllServices");

const getAllServicesController = async (req, res) => {
  let { success, payload, message } = await findAllServices();
  if (success) returnToUser.success(res, "services list", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getAllServicesController;
