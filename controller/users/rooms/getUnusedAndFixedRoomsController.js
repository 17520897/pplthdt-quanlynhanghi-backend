const findUnusedAndFixedRooms = require("../../../functions/rooms/findUnusedAndFixedRooms");

const returnToUser = require("../../../services/returnToUser");

const getUnusedAndFixedRoomsController = async (req, res) => {
  let { success, payload, message } = await findUnusedAndFixedRooms();
  if (success)
    returnToUser.success(res, "list Unused And Fixed rooms", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getUnusedAndFixedRoomsController;
