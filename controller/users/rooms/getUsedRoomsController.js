const findUsedRooms = require("../../../functions/rooms/findUsedRooms");

const returnToUser = require("../../../services/returnToUser");

const getUsedRoomsController = async (req, res) => {
  let { success, payload, message } = await findUsedRooms();
  if (success) returnToUser.success(res, "list used rooms", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getUsedRoomsController;
