const employeesTakeShifts = require("../../../functions/shifts/employeesTakeShifts");

const returnToUser = require("../../../services/returnToUser");

const employeesTakeShiftsController = async (req, res) => {
  let { _id } = req.employees;
  let {
    takeShiftUsername,
    takeShiftPassword,
    timeTranferShift,
    note
  } = req.body;
  let { success, payload, message } = await employeesTakeShifts({
    tranferShiftId: _id,
    takeShiftUsername,
    takeShiftPassword,
    timeTranferShift,
    note
  });
  if (success) returnToUser.success(res, "tranfer shift success", payload);
  else returnToUser.errorWithMess(res, message);
};
module.exports = employeesTakeShiftsController;
