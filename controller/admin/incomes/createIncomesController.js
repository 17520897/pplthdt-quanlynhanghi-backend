const createIncomes = require("../../../functions/incomes/createIncomes");

const returnToUser = require("../../../services/returnToUser");

const createIncomesController = async (req, res) => {
  let { timeGetIncomes, takeMoney, note } = req.body;
  let { success, payload, message } = await createIncomes({
    timeGetIncomes,
    takeMoney,
    note
  });
  if (success) returnToUser.success(res, "incomes success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = createIncomesController;
