const getIncomes = require("../../../functions/incomes/getIncomes");

const returnToUser = require("../../../services/returnToUser");

const getIncomesController = async (req, res) => {
  let { timeGetIncomes } = req.body;
  let { success, payload, messsage } = await getIncomes({ timeGetIncomes });
  if (success) returnToUser.success(res, "get incomes", payload);
  else returnToUser.errorWithMess(res, messsage);
};

module.exports = getIncomesController;
