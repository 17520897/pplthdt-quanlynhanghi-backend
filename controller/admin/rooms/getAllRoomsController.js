const returnToUser = require("../../../services/returnToUser");
const findAllRooms = require("../../../functions/rooms/findAllRooms");

const getAllRoomsController = async (req, res) => {
  let { success, payload, message } = await findAllRooms();
  if (success) returnToUser.success(res, "room list", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getAllRoomsController;
