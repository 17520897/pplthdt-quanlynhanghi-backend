const createRooms = require("../../../functions/rooms/createRooms");
const returnToUser = require("../../../services/returnToUser");

const createRoomsController = async (req, res) => {
  let { success, message, payload } = await createRooms(req.body);
  if (success) returnToUser.success(res, "Create room success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = createRoomsController;
