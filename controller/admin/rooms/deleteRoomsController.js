const deleteRooms = require("../../../functions/rooms/deleteRooms");
const returnToUser = require("../../../services/returnToUser");

const deleteRoomsController = async (req, res) => {
  let { id } = req.params;
  let { success, message } = await deleteRooms({ id });
  if (success) returnToUser.successWithNoData(res, "delete room success");
  else returnToUser.errorWithMess(res, message);
};

module.exports = deleteRoomsController;
