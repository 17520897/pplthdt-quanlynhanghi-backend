const updateRooms = require("../../../functions/rooms/updateRooms");
const returnToUser = require("../../../services/returnToUser");

const updateRoomsController = async (req, res) => {
  let { id } = req.params;
  let { success, message, payload } = await updateRooms(id, req.body);
  if (success) returnToUser.success(res, "update room success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateRoomsController;
