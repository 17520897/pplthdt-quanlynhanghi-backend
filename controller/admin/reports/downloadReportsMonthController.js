const downloadReportsMonth = require("../../../functions/reports/downloadReportsMonth");

const returnToUser = require("../../../services/returnToUser");

const downloadReportsMonthController = async (req, res) => {
  let { month, year } = req.body;
  let { success, payload, message } = await downloadReportsMonth({
    month,
    year
  });
  if (success) returnToUser.success(res, "donwload success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = downloadReportsMonthController;
