const getReportsMonth = require("../../../functions/reports/getReportsMonth");

const returnToUser = require("../../../services/returnToUser");

const getReportsMonthController = async (req, res) => {
  let { month, year } = req.body;
  let { success, payload, message } = await getReportsMonth({ month, year });
  if (success) returnToUser.success(res, "get month reports success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getReportsMonthController;
