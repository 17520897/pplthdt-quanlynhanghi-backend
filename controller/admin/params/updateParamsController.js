const updateParams = require("../../../functions/params/updateParams");
const returnToUser = require("../../../services/returnToUser");
const updateParamsController = async (req, res) => {
  let { success, payload, message } = await updateParams(req.body);
  if (success) returnToUser.success(res, "update params success", payload);
  else returnToUser.errorWithMess(res, "error update params");
};

module.exports = updateParamsController;
