const deleteEmployees = require("../../../functions/employees/deleteEmployees");
const returnToUser = require("../../../services/returnToUser");
const deleteEmployeesController = async (req, res) => {
  let { id } = req.params;
  let { success, payload, message } = await deleteEmployees(id);
  if (success) returnToUser.success(res, "delete employees success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = deleteEmployeesController;
