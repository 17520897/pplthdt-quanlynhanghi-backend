const findAllEmployees = require("../../../functions/employees/findAllEmployees");
const returnToUser = require("../../../services/returnToUser");

const getAllEmployeescontroller = async (req, res) => {
  let { success, payload, message } = await findAllEmployees();
  if (success) returnToUser.success(res, "list employees", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getAllEmployeescontroller;
