const updateEmployees = require("../../../functions/employees/updateEmployees");
const returnToUser = require("../../../services/returnToUser");

const updateEmployeesController = async (req, res) => {
  let { id } = req.params;
  let { success, payload, message } = await updateEmployees(id, req.body);
  if (success) returnToUser.success(res, "update employees success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateEmployeesController;
