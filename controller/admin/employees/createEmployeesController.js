const createEmployees = require("../../../functions/employees/createEmployees");

const returnToUser = require("../../../services/returnToUser");

const createEmployeesController = async (req, res) => {
  let { success, payload, message } = await createEmployees(req.body);
  if (success) returnToUser.success(res, payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = createEmployeesController;
