const updateEmployTakeShift = require("../../../functions/employees/updateEmployeesTakeShift");

const returnToUser = require("../../../services/returnToUser");

const updateEmployeesTakeShiftController = async (req, res) => {
  let { id } = req.params;
  let { _id } = req.admin;
  let { timeTranferShift, note } = req.body;
  let { success, payload, message } = await updateEmployTakeShift(id, {
    adminId: _id,
    timeTranferShift,
    note
  });
  if (success) returnToUser.success(res, "Update take shift success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateEmployeesTakeShiftController;
