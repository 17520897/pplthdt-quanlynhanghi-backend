const findShiftsWithTime = require("../../../functions/shifts/findShiftsWithTime");
const returnToUser = require("../../../services/returnToUser");

const getShiftsWithTimeController = async (req, res) => {
  let { beginTime, endTime } = req.body;
  let { success, payload, message } = await findShiftsWithTime({
    beginTime,
    endTime
  });
  if (success) returnToUser.success(res, "Get shifts with time", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = getShiftsWithTimeController;
