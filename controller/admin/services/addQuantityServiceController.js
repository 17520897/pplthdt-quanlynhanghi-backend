const updateServices = require("../../../functions/services/updateServices");
const returnToUser = require("../../../services/returnToUser");

const addQuantityServicesController = async (req, res) => {
  let { id } = req.params;
  let { quantity } = req.body;
  let { success, message, payload } = await updateServices(id, {
    $inc: { quantity: quantity }
  });
  if (success) returnToUser.success(res, "update services success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = addQuantityServicesController;
