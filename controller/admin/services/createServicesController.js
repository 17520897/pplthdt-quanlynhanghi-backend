const createServices = require("../../../functions/services/createServices");
const returnToUser = require("../../../services/returnToUser");

const createServicesController = async (req, res) => {
  let { success, message, payload } = await createServices(req.body);
  if (success) returnToUser.success(res, "Create services success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = createServicesController;
