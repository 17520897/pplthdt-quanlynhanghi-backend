const updateServices = require("../../../functions/services/updateServices");
const returnToUser = require("../../../services/returnToUser");

const updateRoomsController = async (req, res) => {
  let { id } = req.params;
  let { success, message, payload } = await updateServices(id, req.body);
  if (success) returnToUser.success(res, "update services success", payload);
  else returnToUser.errorWithMess(res, message);
};

module.exports = updateRoomsController;
