const deleteServices = require("../../../functions/services/deleteServices");
const returnToUser = require("../../../services/returnToUser");

const deleteServicesController = async (req, res) => {
  let { id } = req.params;
  let { success, message } = await deleteServices({ id });
  if (success) returnToUser.successWithNoData(res, "delete services success");
  else returnToUser.errorWithMess(res, message);
};

module.exports = deleteServicesController;
