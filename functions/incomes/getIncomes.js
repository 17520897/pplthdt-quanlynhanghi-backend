const mongoose = require("mongoose");
const moment = require("moment");
const getIncomes = async ({ timeGetIncomes }) => {
  try {
    //lấy việc lụm lúa trước đó
    let time = moment(timeGetIncomes).toDate();
    let lastIncomes = await mongoose
      .model("incomes")
      .find({
        time: { $lt: time }
      })
      .sort({ time: -1 })
      .limit(1); //get last time create incomes
    let query;
    let totalMoney = 0;
    if (lastIncomes[0]) {
      totalMoney = lastIncomes[0].currentMoney; //Số tiền được lưu lại gán bằng số tiền còn lại từ lần lấy trước đó
      // nếu có lụm lúa trước đó thì lấy tất cả bills từ lúc lụm lúa trước đó đến bây giờ
      query = {
        checkoutDate: {
          $exists: true,
          $gt: moment(lastIncomes[0].time).toDate(),
          $lte: moment(timeGetIncomes).toDate()
        }
      };
    } // nếu không có thì lấy tất cả tiền từ đầu đến hiện tại
    else {
      query = {
        checkoutDate: {
          $exists: true,
          $lt: time
        }
      };
    }
    // tính tổng số tiền có được từ câu query
    let total = await mongoose.model("bills").aggregate([
      { $match: query },
      {
        $group: { _id: null, totalMoney: { $sum: "$totalPrice" } }
      }
    ]); // trả về array 1 objects
    let bills = await mongoose
      .model("bills")
      .find(query)
      .lean();
    let returnData;
    let totalBillsMoney = 0;
    if (total[0]) {
      // nếu có tồn tại thì số tiền sẽ được cộng vào
      totalMoney += total[0].totalMoney;
      totalBillsMoney = total[0].totalMoney;
    }
    if (lastIncomes[0]) {
      returnData = [
        ...bills,
        {
          lastTimeGetIncomes: moment(lastIncomes[0].time).format(),
          noteLastIncomes: lastIncomes[0].note,
          totalMoneyLastIncomes: lastIncomes[0].totalMoney,
          takeMoneyLastIncomes: lastIncomes[0].takeMoney,
          currentMoneyLastIncomes: lastIncomes[0].currentMoney,
          totalMoney: totalMoney,
          totalBillsMoney: totalBillsMoney
        }
      ];
    } else {
      returnData = [
        ...bills,
        {
          lastTimeGetIncomes: "Không có",
          noteLastIncomes: "Không có",
          totalMoneyLastIncomes: 0,
          takeMoneyLastIncomes: 0,
          currentMoneyLastIncomes: 0,
          totalMoney: totalMoney,
          totalBillsMoney: totalMoney
        }
      ];
    }
    return {
      success: true,
      payload: returnData
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error get incomes"
    };
  }
};

module.exports = getIncomes;
