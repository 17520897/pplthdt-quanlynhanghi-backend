const mongoose = require("mongoose");
const getIncomes = require("./getIncomes");
const createIncomes = async ({ timeGetIncomes, takeMoney, note }) => {
  try {
    let { success, payload, message } = await getIncomes({ timeGetIncomes });
    let totalMoney = payload[payload.length - 1].totalMoney; //objects cuối là objects chứa tổng số tiến
    if (success) {
      if (totalMoney >= takeMoney) {
        let employeesInShift = await mongoose.model("employees").findOne({
          isInShift: true
        });
        let incomes = await mongoose.model("incomes").create({
          totalMoney,
          takeMoney,
          currentMoney: totalMoney - takeMoney,
          employeesInShift: employeesInShift._id,
          nameEmployeesInShift: employeesInShift.name,
          time: timeGetIncomes,
          note
        });
        return {
          success: true,
          payload: incomes
        };
      } else {
        return {
          success: false,
          message: "not enough money"
        };
      }
    } else {
      return {
        success: false,
        message
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create incomes"
    };
  }
};

module.exports = createIncomes;
