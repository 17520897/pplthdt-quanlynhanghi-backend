const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const moment = require("moment");

const employeesTakeShifts = async ({
  tranferShiftId,
  takeShiftUsername,
  takeShiftPassword,
  timeTranferShift,
  note
}) => {
  try {
    // lấy thông tin về nhân viên và account của nhân viên nhận ca
    let employeesInShift = await mongoose
      .model("employees")
      .findById(tranferShiftId);
    let checkAccount = await mongoose
      .model("users")
      .findOne({ username: takeShiftUsername });
    if (
      (!employeesInShift && employeesInShift.isInShift == false) ||
      !checkAccount ||
      !(await bcrypt.compare(takeShiftPassword, checkAccount.password))
    ) {
      //kiểm tra nếu ko có nhân viên trong ca hoặc ko tìm thấy nhân viên nhận ca hoặc account sai account nhân viên nhận ca
      return {
        success: false,
        message: "change shift fail"
      };
    }
    //Lấy thông tin người nhận ca
    let employeesTakeShift = await mongoose.model("employees").findOne({
      accountId: checkAccount._id
    });
    if (_.isEqual(employeesTakeShift._id.toString(), tranferShiftId.toString()))
      return {
        success: false,
        message: "Bạn không thể chuyển ca cho chính mình"
      };
    //Kiểm tra việc trước đó có lụm lúa hay không
    let lastIncomes = await mongoose
      .model("incomes")
      .find({
        time: {
          $lte: moment(timeTranferShift).toDate()
        }
      })
      .sort({ time: -1 })
      .limit(1); //get last time create incomes
    //Lấy thời gian chuyển ca trước đó
    let lastTranferShift = await mongoose
      .model("shifts")
      .find({
        timeTranferShift: {
          $lte: moment(timeTranferShift).toDate()
        }
      })
      .sort({ timeTranferShift: -1 })
      .limit(1); // return array 1 objects
    if (!lastTranferShift[0]) {
      return {
        success: false,
        message: "Chưa có ca nào bắt đầu để chuyển ca"
      };
    }
    let query;
    let totalMoney = 0;
    if (lastIncomes[0]) {
      // nếu trước đó có lụm lúa
      if (
        moment(lastTranferShift[0].timeTranferShift).isBefore(
          moment(lastIncomes[0].time)
        ) // nếu thời gian nhận ca gần đây nhất bé hơn thời gian lụm lúa
      ) {
        totalMoney = lastIncomes[0].currentMoney; // số tiền còn lại lúc lụm lúa
        //lấy bills đã thanh toán từ lúc lụm lúa đến lúc đổi ca
        query = {
          checkoutDate: {
            $exists: true,
            $gt: lastIncomes[0].time,
            $lte: moment(timeTranferShift).toDate()
          }
        };
      } // nếu thời gian chuyển ca trước đó lớn hơn thời gian lụm lúa
      else {
        totalMoney = lastTranferShift[0].currentMoney;
        query = {
          checkoutDate: {
            $exists: true,
            $gt: lastTranferShift[0].timeTranferShift,
            $lte: moment(timeTranferShift).toDate()
          }
        };
      }
    } // nếu trước đó ko có việc lụm lúa thì lấy bills từ lúc nhận ca trước đó đến lúc nhận ca bây giờ
    else {
      query = {
        checkoutDate: {
          $exists: true,
          $gt: lastTranferShift[0].timeTranferShift,
          $lte: moment(timeTranferShift).toDate()
        }
      };
    }
    //tính tổng số tiền đã thanh toán từ query ở trên
    let total = await mongoose.model("bills").aggregate([
      { $match: query },
      {
        $group: { _id: null, totalMoney: { $sum: "$totalPrice" } }
      }
    ]); // trả về array 1 objects
    if (total[0]) {
      // nếu có bills
      totalMoney += total[0].totalMoney;
    }

    //Lấy ca làm theo ngày của employees
    let day = moment(timeTranferShift).day();
    let employeesShiftsLength = employeesTakeShift.shifts.length;
    let shiftsOfTakeShiftEmployees;
    for (let i = 0; i < employeesShiftsLength; i++) {
      if (parseInt(employeesTakeShift.shifts[i].dayOfWeeks) === day) {
        shiftsOfTakeShiftEmployees = employeesTakeShift.shifts[i];
      }
    }
    let data = await mongoose.model("shifts").create({
      tranferShiftId,
      tranferShiftName: employeesInShift.name,
      takeShiftId: employeesTakeShift._id,
      takeShiftName: employeesTakeShift.name,
      timeTranferShift,
      currentMoney: totalMoney,
      note,
      shiftsOfTakeShiftEmployees
    });

    // đổi trạng thái đang trong ca làm việc của 2 nhân viên
    employeesInShift.isInShift = false;
    await employeesInShift.save();
    employeesTakeShift.isInShift = true;
    await employeesTakeShift.save();
    return {
      success: true,
      payload: data
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error employees take shift"
    };
  }
};

module.exports = employeesTakeShifts;
