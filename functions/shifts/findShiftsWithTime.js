const mongoose = require("mongoose");
const moment = require("moment");
const findShiftsWithTime = async ({ beginTime, endTime }) => {
  try {
    beginTime = moment(beginTime).toDate();
    endTime = moment(endTime).toDate();
    let shifts = await mongoose.model("shifts").find({
      timeTranferShift: {
        $gte: beginTime,
        $lte: endTime
      }
    });
    return {
      success: true,
      payload: shifts
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "Server error find all shifts"
    };
  }
};

module.exports = findShiftsWithTime;
