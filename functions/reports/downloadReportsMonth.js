const mongoose = require("mongoose");
const getReportsMonth = require("./getReportsMonth");
const fs = require("async-file");
const xl = require("excel4node");
const moment = require("moment");

const downloadReportsMonth = async ({ month, year }) => {
  try {
    let { success, payload, message } = await getReportsMonth({
      month,
      year
    });
    if (success) {
      let wb = new xl.Workbook();
      let options = {
        sheetFormat: {
          baseColWidth: 20
        }
      };
      let myStyle = wb.createStyle({
        alignment: {
          wrapText: true
        }
      });
      let redColor = wb.createStyle({
        font: {
          color: "#FF0000",
          bold: true
        }
      });
      let ws = wb.addWorksheet("Reports", options);
      ws.cell(1, 1, 1, 3, true).string(`Tổng tiền tháng ${month}/${year}: `);
      ws.cell(1, 4)
        .string(payload.monthTotalMoney.toString())
        .style(redColor);
      ws.cell(3, 1).string("Thời gian lấy tiền");
      ws.cell(3, 2).string("Nhân viên trong ca");
      ws.cell(3, 3).string("Tổng số tiền");
      ws.cell(3, 4).string("Số tiền đã lấy");
      ws.cell(3, 5).string("Số tiền còn lại");
      ws.cell(3, 6).string("Ghi chú");

      let incomesLength = payload.incomes.length;
      let incomes = payload.incomes;
      for (let i = 0; i < incomesLength; i++) {
        ws.cell(i + 4, 1).string(
          moment(incomes[i].time)
            .format("HH:mm DD-MM-YYYY")
            .toString()
        );
        ws.cell(i + 4, 2).string(incomes[i].nameEmployeesInShift);
        ws.cell(i + 4, 3).string(incomes[i].totalMoney.toString());
        ws.cell(i + 4, 4).string(incomes[i].takeMoney.toString());
        ws.cell(i + 4, 5).string(incomes[i].currentMoney.toString());
        ws.cell(i + 4, 6)
          .string(incomes[i].note)
          .style(myStyle);
      }

      let buffer = await wb.writeToBuffer();
      let filePath = "public/reports/reports.xlsx";
      await fs.writeFile(filePath, buffer, {
        encoding: "utf8"
      });
      return {
        success: true,
        payload: `${process.env.SERVER_URL}/reports/reports.xlsx`
      };
    } else {
      return {
        success: false,
        message: message
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error download reports month"
    };
  }
};

module.exports = downloadReportsMonth;
