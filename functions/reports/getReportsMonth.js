const mongoose = require("mongoose");

const moment = require("moment");

const getReportsMonth = async ({ month, year }) => {
  try {
    let beginTime = moment(`${year}-${month}-01T00:00:00`).toDate();
    let daysInMonth = moment(
      `${year}-${month}T00:00:00`,
      "YYYY-MM"
    ).daysInMonth();
    let endTime = moment(`${year}-${month}-${daysInMonth}`).toDate();

    let data = {
      monthTotalMoney: 0,
      incomes: []
    };
    let billQuery = {
      checkoutDate: {
        $gte: beginTime,
        $lte: endTime
      }
    };
    let incomesQuery = {
      time: {
        $gte: beginTime,
        $lte: endTime
      }
    };
    let total = await mongoose.model("bills").aggregate([
      { $match: billQuery },
      {
        $group: { _id: null, totalMoney: { $sum: "$totalPrice" } }
      }
    ]);
    let incomes = await mongoose
      .model("incomes")
      .find(incomesQuery)
      .select(
        "-_id + time nameEmployeesInShift totalMoney takeMoney currentMoney note"
      );
    if (total[0]) {
      data = {
        ...data,
        monthTotalMoney: total[0].totalMoney
      };
    }

    data.incomes = incomes;
    return {
      success: true,
      payload: data
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error get reports with time"
    };
  }
};

module.exports = getReportsMonth;
