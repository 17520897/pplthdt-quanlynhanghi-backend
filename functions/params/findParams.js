const mongoose = require("mongoose");

const findParams = async () => {
  try {
    let params = await mongoose.model("params").findOne({});
    return {
      success: true,
      payload: params
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find params"
    };
  }
};

module.exports = findParams;
