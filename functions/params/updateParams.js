const mongoose = require("mongoose");

const updateParams = async updateData => {
  try {
    let params = await mongoose
      .model("params")
      .findOneAndUpdate({}, updateData, { new: true });
    return {
      success: true,
      payload: params
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update params"
    };
  }
};

module.exports = updateParams;
