const mongoose = require("mongoose");
const deleteUsers = require("../users/deleteUsers");

const deleteEmployees = async id => {
  try {
    let employees = await mongoose
      .model("employees")
      .findById(id)
      .lean();
    let { success, message } = await deleteUsers(employees.accountId);
    if (success) {
      await mongoose.model("employees").findByIdAndDelete(id);
      return {
        success: true,
        message: "delete employees success"
      };
    } else {
      return {
        success: false,
        message: message
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error delete employees"
    };
  }
};

module.exports = deleteEmployees;
