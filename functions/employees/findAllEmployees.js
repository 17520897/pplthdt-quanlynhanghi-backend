const mongoose = require("mongoose");
const findOneUsers = require("../users/findOneUsers");
const findAllEmployees = async () => {
  try {
    let employees = await mongoose
      .model("employees")
      .find()
      .lean();
    let employeesLength = employees.length;
    for (let i = 0; i < employeesLength; i++) {
      let { success, payload } = await findOneUsers({
        id: employees[i].accountId
      });
      if (success) {
        employees[i].username = payload.username;
      } else {
        return {
          success: false,
          message: "server error process find all users"
        };
      }
    }
    return {
      success: true,
      payload: employees
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "error find all employees"
    };
  }
};

module.exports = findAllEmployees;
