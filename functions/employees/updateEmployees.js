const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const updateEmployees = async (id, updateData) => {
  try {
    let { identity, password, oldPassword } = updateData;
    checkEmployees = await mongoose.model("employees").findOne({
      $and: [{ identity }, { _id: { $ne: id } }]
    });
    if (checkEmployees) {
      return {
        success: false,
        message: "employees identity existed"
      };
    } else {
      if (password) {
        let employees = await mongoose
          .model("employees")
          .findById(id)
          .lean();
        let user = await mongoose.model("users").findById(employees.accountId);
        if (await bcrypt.compare(oldPassword, user.password)) {
          user.password = await bcrypt.hash(password, 10);
          await user.save();
        } else {
          return {
            success: false,
            message: "Wrong old password"
          };
        }
      }
      let employees = await mongoose
        .model("employees")
        .findByIdAndUpdate(id, updateData, {
          new: true
        });
      return {
        success: true,
        payload: employees
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update employees"
    };
  }
};

module.exports = updateEmployees;
