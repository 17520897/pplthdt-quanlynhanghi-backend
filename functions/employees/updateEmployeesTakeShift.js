const mongoose = require("mongoose");
const moment = require("moment");
const updateEmployeesTakeShift = async (
  id,
  { adminId, timeTranferShift, note }
) => {
  try {
    //đặt time tranfershift thành moment
    timeTranferShift = moment(timeTranferShift).format();
    await mongoose.model("employees").updateMany(
      {},
      {
        $set: { isInShift: false }
      }
    );
    //update chuyển ca cho nhân viên
    let employees = await mongoose.model("employees").findByIdAndUpdate(
      id,
      {
        $set: { isInShift: true }
      },
      { new: true }
    );
    if (!employees) return { success: false, message: "employees not found" };
    //Lấy ca làm việc của nhân viên
    let day = moment(timeTranferShift).day();
    let employeesShiftsLenght = employees.shifts.length;
    let shiftsOfTakeShiftEmployees;
    for (let i = 0; i < employeesShiftsLenght; i++) {
      if (parseInt(employees.shifts[i].dayOfWeeks) === day) {
        shiftsOfTakeShiftEmployees = employees.shifts[i];
      }
    }
    //tính toán số tiền
    let query;
    let totalMoney = 0;
    let lastIncomes = await mongoose
      .model("incomes")
      .find({
        time: { $lte: moment(timeTranferShift).toDate() }
      })
      .sort({ time: -1 })
      .limit(1); //get last time create incomes
    let lastTranferShift = await mongoose
      .model("shifts")
      .find({
        timeTranferShift: {
          $lte: moment(timeTranferShift).toDate()
        }
      })
      .sort({ timeTranferShift: -1 })
      .limit(1); // return array 1 objects
    if (!lastTranferShift[0]) {
      //nếu trước đó chưa có ca làm việc thì chưa có tiền
      let data = await mongoose.model("shifts").create({
        tranferShiftId: adminId,
        tranferShiftName: "admin",
        takeShiftId: employees._id,
        takeShiftName: employees.name,
        timeTranferShift,
        note,
        currentMoney: 0,
        shiftsOfTakeShiftEmployees
      });
      return {
        success: true,
        payload: data
      };
    }
    if (lastIncomes[0]) {
      //nếu trước đó có lấy tiền
      if (
        moment(lastTranferShift[0].timeTranferShift).isBefore(
          moment(lastIncomes[0].time)
        ) // nếu thời giữa 2 thời gian chuyển ca có lấy tiền
      ) {
        totalMoney = lastIncomes[0].currentMoney; // gán số tiền còn lại bằng với số tiền giữa 2 lần chuyển ca
        //lấy tất cả bills đã thanh toán giữa lần lấy tiền và lần chuyển ca
        query = {
          checkoutDate: {
            $exists: true,
            $gt: moment(lastIncomes[0].time).toDate(),
            $lte: moment(timeTranferShift).toDate()
          }
        };
      } // thời gian lấy tiền trước đó không nằm trong ca
      else {
        // lấy tất cả bills đã thanh toán giữa 2 lần chuyển ca
        query = {
          checkoutDate: {
            $exists: true,
            $gt: moment(lastTranferShift[0].timeTranferShift).toDate(),
            $lte: moment(timeTranferShift).toDate()
          }
        };
      }
    } // nếu trước đó chưa có lần lấy tiền nào
    else {
      // nếu trước đó chưa lần tiền thì số tiền sẽ là tổng bills từ đầu
      query = {
        checkoutDate: {
          $exists: true,
          $lte: moment(timeTranferShift).toDate()
        }
      };
    }
    let total = await mongoose.model("bills").aggregate([
      { $match: query },
      {
        $group: { _id: null, totalMoney: { $sum: "$totalPrice" } }
      }
    ]); // trả về array 1 objects
    console.log(totalMoney);
    if (total[0]) {
      totalMoney += total[0].totalMoney;
    } //if have some bill when tranfer shift plus this money to totalMoney

    //Tạo chuyển ca
    let data = await mongoose.model("shifts").create({
      tranferShiftId: adminId,
      tranferShiftName: "admin",
      takeShiftId: employees._id,
      takeShiftName: employees.name,
      timeTranferShift,
      note,
      currentMoney: totalMoney,
      shiftsOfTakeShiftEmployees
    });
    return {
      success: true,
      payload: data
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update employees take shift"
    };
  }
};

module.exports = updateEmployeesTakeShift;
