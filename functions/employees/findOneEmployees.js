const mongoose = require("mongoose");

const findOneEmployees = async ({ id, identity }) => {
  try {
    let query;
    if (identity) {
      query = { identity };
    }
    if (id) {
      query = { _id: id };
    }
    let employees = await mongoose.model("employees").findOne(query);
    if (employees) {
      return {
        success: true,
        payload: employees
      };
    } else
      return {
        success: false,
        message: "employees not existed"
      };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find one employees"
    };
  }
};

module.exports = findOneEmployees;
