const mongoose = require("mongoose");
const createUsers = require("../users/createUsers");

const createEmployees = async data => {
  try {
    let checkEmployees = await mongoose
      .model("employees")
      .findOne({ identity: data.identity });
    if (checkEmployees)
      return {
        success: false,
        message: "employees identity existed"
      };
    let { username, password } = data;
    let { success, payload, message } = await createUsers({
      username,
      password,
      role: "user"
    });
    if (success) {
      data.accountId = payload._id;
      let employees = await mongoose.model("employees").create(data);
      if (employees)
        return {
          success: true,
          payload: employees
        };
    } else {
      return {
        success: false,
        message: message
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create employees"
    };
  }
};

module.exports = createEmployees;
