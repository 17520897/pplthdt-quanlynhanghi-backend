const mongoose = require("mongoose");

const findOneUsers = async ({ username, id }) => {
  try {
    let query;
    if (username) {
      query = { username };
    }
    if (id) {
      query = { _id: id };
    }
    let user = await mongoose.model("users").findOne(query);
    if (user) {
      return {
        success: true,
        payload: user
      };
    } else {
      return {
        success: false,
        message: "Không tìm thấy user"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find user"
    };
  }
};

module.exports = findOneUsers;
