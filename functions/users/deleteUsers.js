const mongoose = require("mongoose");

const deleteUsers = async id => {
  try {
    await mongoose.model("users").findByIdAndDelete(id);
    return {
      success: true,
      message: "delete users success"
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error delete users"
    };
  }
};

module.exports = deleteUsers;
