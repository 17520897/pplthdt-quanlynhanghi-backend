const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const findOneUsers = require("./findOneUsers");
const createUsers = async ({ username, password, role }) => {
  try {
    let { success, message } = await findOneUsers({ username });
    if (success == false) {
      if (role !== "admin") role = "user";
      password = await bcrypt.hash(password, 10);
      let user = await mongoose.model("users").create({
        username,
        password,
        role
      });
      if (user) {
        return {
          success: true,
          payload: user
        };
      }
    } else {
      return {
        success: false,
        message: "User existed"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create users"
    };
  }
};

module.exports = createUsers;
