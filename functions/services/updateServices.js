const mongoose = require("mongoose");

const updateServices = async (id, updateData) => {
  try {
    let canUpdate = true;
    if (updateData.name) {
      checkServices = await mongoose.model("services").findOne({
        $and: [{ name: updateData.name }, { _id: { $ne: id } }]
      });
      if (checkServices) {
        canUpdate = false;
        return {
          success: false,
          message: "Services name existed"
        };
      }
    }
    if (canUpdate) {
      let services = await mongoose
        .model("services")
        .findByIdAndUpdate(id, updateData)
        .lean();
      if (services) {
        services = {
          ...services,
          ...updateData
        };
        return {
          success: true,
          payload: services
        };
      } else {
        return {
          success: false,
          message: "service not existed"
        };
      }
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update servuce"
    };
  }
};

module.exports = updateServices;
