const mongoose = require("mongoose");

const findAllServices = async () => {
  try {
    let services = await mongoose.model("services").find();
    return {
      success: true,
      payload: services
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find all services"
    };
  }
};

module.exports = findAllServices;
