const mongoose = require("mongoose");

const deleteServices = async ({ id }) => {
  try {
    await mongoose.model("services").findByIdAndDelete(id);
    return {
      success: true,
      message: "Delete services success"
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error delete services"
    };
  }
};

module.exports = deleteServices;
