const mongoose = require("mongoose");

const findOneServices = require("./findOneServices");

const createServices = async ({ name, price, quantity }) => {
  try {
    let { success, message } = await findOneServices({ serviceName: name });
    if (success == false) {
      let service = await mongoose.model("services").create({
        name,
        price,
        quantity
      });
      if (service) {
        return {
          success: true,
          payload: service
        };
      }
    } else {
      return {
        success: false,
        message: "service name existed"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create room"
    };
  }
};

module.exports = createServices;
