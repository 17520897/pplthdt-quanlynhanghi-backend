const mongoose = require("mongoose");

const findOneServices = async ({ serviceName, id }) => {
  try {
    let query;
    if (serviceName) query = { name: serviceName };
    if (id) query = { _id: id };
    let service = await mongoose.model("services").findOne(query);
    if (service)
      return {
        success: true,
        payload: service
      };
    else
      return {
        success: false,
        message: "Services existed"
      };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find one service"
    };
  }
};

module.exports = findOneServices;
