const mongoose = require("mongoose");

const deleteRooms = async ({ id }) => {
  try {
    let rooms = await mongoose.model("rooms").findById(id);
    if (rooms.status === "used")
      return {
        success: false,
        message: "rooms used can't delete"
      };
    else {
      await mongoose.model("rooms").findByIdAndDelete(id);
      return {
        success: true,
        message: "Delete room success"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error delete room"
    };
  }
};

module.exports = deleteRooms;
