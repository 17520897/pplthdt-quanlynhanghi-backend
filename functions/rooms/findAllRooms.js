const mongoose = require("mongoose");
const _ = require("lodash");
const findAllRooms = async () => {
  try {
    let rooms = await mongoose
      .model("rooms")
      .find({
        status: { $ne: "used" }
      })
      .lean();
    let usedRooms = await mongoose
      .model("rooms")
      .find({
        status: "used"
      })
      .lean();
    for (let i = 0; i < usedRooms.length; i++) {
      let bill = await mongoose.model("bills").findOne({
        roomId: usedRooms[i]._id,
        checkoutDate: { $exists: false }
      });
      let {
        customerName,
        customerIdentity,
        checkinDate,
        services,
        rentType,
        _id
      } = bill;
      _.assign(usedRooms[i], {
        customerName,
        customerIdentity,
        checkinDate,
        services,
        rentType,
        billId: _id
      });
    }
    return {
      success: true,
      payload: [...rooms, ...usedRooms]
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find all rooms"
    };
  }
};

module.exports = findAllRooms;
