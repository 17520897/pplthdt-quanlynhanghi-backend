const mongoose = require("mongoose");

const findOneRooms = require("./findOneRooms");

const createRooms = async ({ name, type, status }) => {
  try {
    let { success, message } = await findOneRooms({ roomsName: name.trim() });
    if (success == false) {
      if (type !== "cold") type = "common";
      if (status !== "fixed") status = "unused";
      let room = await mongoose.model("rooms").create({
        name: name.trim(),
        type,
        status
      });
      if (room) {
        return {
          success: true,
          payload: room
        };
      }
    } else {
      return {
        success: false,
        message: "Room name existed"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create room"
    };
  }
};

module.exports = createRooms;
