const mongoose = require("mongoose");

const findOneRooms = async ({ roomsName, id }) => {
  try {
    let query;
    if (roomsName) query = { name: roomsName };
    if (id) query = { _id: id };
    let room = await mongoose.model("rooms").findOne(query);
    if (room)
      return {
        success: true,
        payload: room
      };
    else
      return {
        success: false,
        message: "Room existed"
      };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find one room"
    };
  }
};

module.exports = findOneRooms;
