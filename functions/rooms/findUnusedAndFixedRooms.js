const mongoose = require("mongoose");

const findUnusedAndFixedRooms = async () => {
  try {
    let rooms = await mongoose.model("rooms").find({
      status: { $ne: "used" }
    });
    return {
      success: true,
      payload: rooms
    };
  } catch (err) {
    console(err);
    return {
      success: false,
      message: "server error find unused and fixed rooms"
    };
  }
};

module.exports = findUnusedAndFixedRooms;
