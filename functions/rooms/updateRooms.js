const mongoose = require("mongoose");

const updateRooms = async (id, updateData) => {
  try {
    let checkRooms = await mongoose.model("rooms").findOne({
      $and: [{ name: updateData.name }, { _id: { $ne: id } }]
    });
    if (checkRooms) {
      return {
        success: false,
        message: "Room name existed can't update"
      };
    }
    checkRooms = await mongoose.model("rooms").findById(id);
    if (checkRooms.status === "used") {
      return {
        success: false,
        message: "Room used can't update"
      };
    } else {
      let { status } = updateData;
      let statusArray = ["used", "unused", "fixed"];
      if (statusArray.indexOf(status) === -1) {
        updateData.status = "unused";
      }
      let room = await mongoose
        .model("rooms")
        .findByIdAndUpdate(id, updateData)
        .lean();
      if (room) {
        room = {
          ...room,
          ...updateData
        };
        return {
          success: true,
          payload: room
        };
      } else {
        return {
          success: false,
          message: "Room not existed"
        };
      }
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update room"
    };
  }
};

module.exports = updateRooms;
