const mongoose = require("mongoose");
const _ = require("lodash");
const findUsedRooms = async () => {
  try {
    let rooms = await mongoose
      .model("rooms")
      .find({
        status: "used"
      })
      .lean();
    for (let i = 0; i < rooms.length; i++) {
      let bill = await mongoose
        .model("bills")
        .findOne({
          roomId: rooms[i]._id,
          checkoutDate: { $exists: false }
        })
        .lean();
      let {
        customerName,
        customerIdentity,
        checkinDate,
        services,
        rentType,
        _id
      } = bill;

      _.assign(rooms[i], {
        customerName,
        customerIdentity,
        checkinDate,
        services,
        rentType,
        billId: _id
      });
    }
    return {
      success: true,
      payload: rooms
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find used rooms"
    };
  }
};

module.exports = findUsedRooms;
