const mongoose = require("mongoose");

const updateServices = async (billId, services) => {
  try {
    let bill = await mongoose.model("bills").findById(billId);
    if (bill) {
      let canUpdate = true;
      for (let i = 0; i < services.length; i++) {
        let service = await mongoose
          .model("services")
          .findOne({ name: services[i].serviceName });
        if (service) {
          if (services[i].quantity > service.quantity) {
            canUpdate = false;
          }
        } else canUpdate = false;
      }
      if (canUpdate) {
        for (let i = 0; i < services.length; i++) {
          let service = await mongoose
            .model("services")
            .findOne({ name: services[i].serviceName });
          service.quantity -= services[i].quantity;
          await service.save();
        }
        bill.services = services;
        await bill.save();
        return {
          success: true,
          payload: bill
        };
      } else {
        return {
          success: false,
          message: "service wrong of not enough"
        };
      }
    } else {
      return {
        success: false,
        message: "bill not exists"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update services"
    };
  }
};

module.exports = updateServices;
