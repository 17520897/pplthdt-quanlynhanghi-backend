const mongoose = require("mongoose");
const moment = require("moment");
const paidBills = async ({ billId, checkoutDate }) => {
  try {
    let bill = await mongoose.model("bills").findOne({
      $and: [{ _id: billId }, { checkoutDate: { $exists: false } }]
    });
    if (
      !checkoutDate ||
      moment(checkoutDate).isBefore(moment(bill.checkinDate))
    ) {
      return {
        success: false,
        message: "checkoutDate can't null or lower than checkin time"
      };
    }
    if (bill) {
      let { rentType, pricePer12Hour, pricePerHour, roomId } = bill;
      //calculate time
      let params = await mongoose.model("params").findOne({});
      let checkoutTime = moment(checkoutDate).subtract(params.extraTime, "m");
      let checkinTime = moment(bill.checkinDate);
      let rentHour = Math.ceil(
        moment.duration(checkoutTime.diff(checkinTime)).asHours()
      );
      bill.rentHour = rentHour;
      bill.checkoutDate = checkoutDate.toString();
      //calculate total price
      let totalPrice = 0;
      let totalServicePrice = 0;
      let serviceLength = bill.services.length;
      for (let i = 0; i < serviceLength; i++) {
        totalServicePrice += bill.services[i].price * bill.services[i].quantity;
      }
      if (rentType === "12Hour") {
        let rentHourOver12 = rentHour > 12 ? rentHour - 12 : 0;
        totalPrice =
          pricePer12Hour + pricePerHour * rentHourOver12 + totalServicePrice;
      } else {
        totalPrice = pricePerHour * rentHour + totalServicePrice;
      }
      bill.totalPrice = totalPrice;
      await bill.save();
      await mongoose
        .model("rooms")
        .findByIdAndUpdate(roomId, { $set: { status: "unused" } });
      return {
        success: true,
        payload: bill
      };
    } else
      return {
        success: false,
        message: "bill not existed"
      };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error paid bills"
    };
  }
};

module.exports = paidBills;
