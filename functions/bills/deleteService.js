const mongoose = require("mongoose");

const deleteService = async (billId, serviceName) => {
  try {
    let bill = await mongoose
      .model("bills")
      .findOne(
        { _id: billId, "services.serviceName": serviceName },
        { _id: 0, services: { $elemMatch: { serviceName } } }
      );
    if (bill) {
      let quantity = bill.services[0].quantity;
      bill = await mongoose
        .model("bills")
        .findByIdAndUpdate(
          billId,
          { $pull: { services: { serviceName } } },
          { safe: true, multi: false, new: true }
        );
      await mongoose
        .model("services")
        .findOneAndUpdate({ name: serviceName }, { $inc: { quantity } });
      return {
        success: true,
        payload: bill
      };
    } else {
      return {
        success: false,
        message: "services not exists"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error update services"
    };
  }
};

module.exports = deleteService;
