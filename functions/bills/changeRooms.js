const mongoose = require("mongoose");
const _ = require("lodash");

const changeRooms = async (billId, roomId) => {
  try {
    let bill = await mongoose.model("bills").findById(billId);
    let checkRooms = await mongoose.model("rooms").findById(roomId);
    if (checkRooms) {
      if (bill) {
        //update rooms
        await mongoose.model("rooms").findByIdAndUpdate(
          { _id: bill.roomId },
          {
            $set: { status: "unused" }
          }
        );
        let rooms = await mongoose.model("rooms").findByIdAndUpdate(
          { _id: roomId },
          {
            $set: { status: "used" }
          }
        );
        //update price and bill
        let { name, type } = rooms;
        let { checkinDate } = bill;
        let Day = new Date(checkinDate).getDay();
        let params = await mongoose
          .model("params")
          .findOne()
          .lean();
        let pricePerHour = 0,
          pricePer12Hour = 0;
        if (
          params.setTodayIsHoliday ||
          (params.setWeekendIsHoliday && (Day === 6 || Day === 0))
        ) {
          /*holiday*/ if (type === "common") {
            pricePerHour = params.pricePerHourHolidayCommonRoom;
            pricePer12Hour = params.pricePer12HourHolidayCommonRoom;
          } else {
            pricePerHour = params.pricePerHourHolidayColdRoom;
            pricePer12Hour = params.pricePer12HourHolidayColdRoom;
          }
        } /*common day*/ else {
          if (type === "common") {
            pricePerHour = params.pricePerHourCommonDayCommonRoom;
            pricePer12Hour = params.pricePer12HourCommonDayCommonRoom;
          } else {
            pricePerHour = params.pricePerHourCommonDayColdRoom;
            pricePer12Hour = params.pricePer12HourCommonDayColdRoom;
          }
        }
        _.assign(bill, {
          roomName: name,
          roomType: type,
          roomId,
          pricePerHour,
          pricePer12Hour
        });
        await bill.save();
        return {
          success: true,
          payload: bill
        };
      } else
        return {
          success: false,
          message: "bill not exists"
        };
    } else {
      return {
        success: false,
        message: "room not exists"
      };
    }
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error change rooms"
    };
  }
};

module.exports = changeRooms;
