const mongoose = require("mongoose");
const moment = require("moment");
const createBills = async data => {
  try {
    let { employeesId, roomId, checkinDate, rentType } = data;
    //delete checkoutDate if exists
    delete data.checkoutDate;
    //room
    let room = await mongoose.model("rooms").findOne({
      _id: roomId,
      status: "unused"
    });
    if (!room) {
      return {
        success: false,
        message: "room not in status unused"
      };
    }
    data.roomName = room.name;
    data.roomType = room.type;
    //day
    let { roomType } = data;
    let Day = moment(checkinDate).day();
    let params = await mongoose
      .model("params")
      .findOne()
      .lean();
    let pricePerHour = 0,
      pricePer12Hour = 0;
    if (
      params.setTodayIsHoliday ||
      (params.setWeekendIsHoliday && (Day === 6 || Day === 0))
    ) {
      /*holiday*/ if (roomType === "common") {
        pricePerHour = params.pricePerHourHolidayCommonRoom;
        pricePer12Hour = params.pricePer12HourHolidayCommonRoom;
      } else {
        pricePerHour = params.pricePerHourHolidayColdRoom;
        pricePer12Hour = params.pricePer12HourHolidayColdRoom;
      }
    } /*common day*/ else {
      if (roomType === "common") {
        pricePerHour = params.pricePerHourCommonDayCommonRoom;
        pricePer12Hour = params.pricePer12HourCommonDayCommonRoom;
      } else {
        pricePerHour = params.pricePerHourCommonDayColdRoom;
        pricePer12Hour = params.pricePer12HourCommonDayColdRoom;
      }
    }
    data.pricePerHour = pricePerHour;
    data.pricePer12Hour = pricePer12Hour;
    //employees
    let employees = await mongoose
      .model("employees")
      .findById(employeesId)
      .lean();
    if (!employees) {
      return {
        success: false,
        message: "employees not existed"
      };
    }
    data.employeesName = employees.name;
    //rentType
    if (rentType !== "12Hour") rentType = "perHour";
    //Chuyển checkin date bills sang dạng moment để lưu
    data.checkinDate = moment(data.checkinDate).format();
    //create
    let bill = await mongoose.model("bills").create(data);
    room.status = "used";
    await room.save();
    return {
      success: true,
      payload: bill
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error create bill"
    };
  }
};

module.exports = createBills;
