const mongoose = require("mongoose");

const findNotPaidBills = async () => {
  try {
    let bills = await mongoose.model("bills").find({
      checkoutDate: { $exists: false }
    });
    return {
      success: true,
      payload: bills
    };
  } catch (err) {
    console.log(err);
    return {
      success: false,
      message: "server error find not paid bills"
    };
  }
};

module.exports = findNotPaidBills;
