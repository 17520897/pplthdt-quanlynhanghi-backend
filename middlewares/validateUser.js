const { isEmpty } = require("lodash");
const jwt = require("jsonwebtoken");

const validateUser = (req, res, next) => {
  try {
    if (req.headers.authorization) {
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.SECRET_KEY
      );
      if (payload.isAdmin == false) {
        req.employees = payload;
        return next();
      } else {
        return res.status(401).json({
          message: "You are not authorized"
        });
      }
    } else {
      return res.status(401).json({
        message: "You are not authorized"
      });
    }
  } catch (err) {
    console.log(err);
    return res.status(401).json({
      message: "You are not authorized"
    });
  }
};

module.exports = validateUser;
