const { isEmpty } = require("lodash");
const jwt = require("jsonwebtoken");

const validateAdmin = () => (req, res, next) => {
  try {
    if (req.headers.authorization) {
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.SECRET_KEY
      );
      if (payload.isAdmin) {
        req.admin = payload;
        return next();
      } else {
        return res.status(401).json({
          message: "You are not authorized"
        });
      }
    } else {
      return res.status(401).json({
        message: "You are not authorized"
      });
    }
  } catch (e) {
    if (optional && isEmpty(req.headers.authorization)) return next();
    return res.status(401).json({
      message: "You are not authorized"
    });
  }
};

module.exports = validateAdmin;
