const { isEmpty } = require("lodash");
const jwt = require("jsonwebtoken");
const findOneEmployees = require("../functions/employees/findOneEmployees");

const validateTakeShiftUsers = async (req, res, next) => {
  try {
    let { _id } = req.employees;
    let { success, payload, message } = await findOneEmployees({ id: _id });
    if (success && payload.isInShift) next();
    else
      return res.status(403).json({
        message: "You are not in shift"
      });
  } catch (e) {
    return res.status(403).json({
      message: "You are not in shift"
    });
  }
};

module.exports = validateTakeShiftUsers;
