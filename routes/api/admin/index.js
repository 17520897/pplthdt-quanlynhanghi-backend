const router = require("express").Router();
const refresh = require("../../../controller/admin/tokens/refresh");
const validateAdmin = require("../../../middlewares/validateAdmin");

router.get("/refresh", refresh);
router.use(validateAdmin({ optional: false }));
router.use("/rooms", require("./rooms"));
router.use("/services", require("./services"));
router.use("/params", require("./params"));
router.use("/employees", require("./employees"));
router.use("/incomes", require("./incomes"));
router.use("/shifts", require("./shifts"));
router.use("/reports", require("./reports"));

module.exports = router;
