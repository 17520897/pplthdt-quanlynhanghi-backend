const router = require("express").Router();

const createServicesController = require("../../../../controller/admin/services/createServicesController");
const deleteServicesController = require("../../../../controller/admin/services/deleteServicesController");
const getAllServicesController = require("../../../../controller/admin/services/getAllServicesController");
const updateServicesController = require("../../../../controller/admin/services/updateServicesController");
const addQuantityServicesController = require("../../../../controller/admin/services/addQuantityServiceController");

router.post("/", createServicesController);
router.delete("/:id", deleteServicesController);
router.get("/", getAllServicesController);
router.put("/:id", updateServicesController);
router.put("/quantity/:id", addQuantityServicesController);

module.exports = router;
