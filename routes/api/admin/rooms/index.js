const router = require("express").Router();

const createRoomsController = require("../../../../controller/admin/rooms/createRoomsController");
const deleteRoomsController = require("../../../../controller/admin/rooms/deleteRoomsController");
const getAllRoomsController = require("../../../../controller/admin/rooms/getAllRoomsController");
const updateRoomsController = require("../../../../controller/admin/rooms/updateRoomsController");
const getUsedRoomsController = require("../../../../controller/admin/rooms/getUsedRoomsController");
const getUnusedAndFixedRoomsController = require("../../../../controller/admin/rooms/getUnusedAndFixedRoomsController");

router.get("/", getAllRoomsController);
router.get("/used", getUsedRoomsController);
router.get("/unsed", getUnusedAndFixedRoomsController);

router.put("/:id", updateRoomsController);

router.post("/", createRoomsController);

router.delete("/:id", deleteRoomsController);

module.exports = router;
