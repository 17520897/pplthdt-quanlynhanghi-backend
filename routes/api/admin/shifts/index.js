const router = require("express").Router();

const getShiftsWithTimeController = require("../../../../controller/admin/shifts/getShiftsWithTimeController");

router.post("/time", getShiftsWithTimeController);

module.exports = router;
