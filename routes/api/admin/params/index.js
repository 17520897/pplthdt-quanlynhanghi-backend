const router = require("express").Router();

const updateParamsController = require("../../../../controller/admin/params/updateParamsController");
const getParamsController = require("../../../../controller/admin/params/getParamsController");

router.put("/", updateParamsController);
router.get("/", getParamsController);

module.exports = router;
