const router = require("express").Router();

const getIncomesController = require("../../../../controller/admin/incomes/getIncomesController");
const createIncomesController = require("../../../../controller/admin/incomes/createIncomesController");

router.post("/", getIncomesController);

router.post("/takeMoney", createIncomesController);

module.exports = router;
