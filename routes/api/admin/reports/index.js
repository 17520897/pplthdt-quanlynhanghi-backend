const router = require("express").Router();

const getReportsMonthController = require("../../../../controller/admin/reports/getReportsMonthController");
const downloadReportsMonthController = require("../../../../controller/admin/reports/downloadReportsMonthController");

router.post("/month", getReportsMonthController);
router.post("/month/download", downloadReportsMonthController);

module.exports = router;
