const router = require("express").Router();

const createEmployeesController = require("../../../../controller/admin/employees/createEmployeesController");
const updateEmployeesController = require("../../../../controller/admin/employees/updateEmployeesController");
const getAllEmployeesController = require("../../../../controller/admin/employees/getAllEmployeesController");
const deleteEmployeesController = require("../../../../controller/admin/employees/deleteEmployeesController");
const updateEmployeesTakeShiftController = require("../../../../controller/admin/employees/updateEmployeesTakeShiftController");

router.post("/", createEmployeesController);
router.put("/:id", updateEmployeesController);
router.put("/:id/takeShift", updateEmployeesTakeShiftController);
router.get("/", getAllEmployeesController);
router.delete("/:id", deleteEmployeesController);

module.exports = router;
