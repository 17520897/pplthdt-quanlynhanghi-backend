const router = require("express").Router();
const createBillsController = require("../../../../controller/users/bills/createBillsController");
const paidBillsController = require("../../../../controller/users/bills/paidBillsController");

const changeRoomsController = require("../../../../controller/users/bills/changeRoomsController");
const updateServicesController = require("../../../../controller/users/bills/updateServicesController");
const deleteServiceController = require("../../../../controller/users/bills/deleteServiceController");
const getOneBillsController = require("../../../../controller/users/bills/getOneBillsController");

router.post("/", createBillsController);
router.post("/paid/:id", paidBillsController);
router.put("/services/:id", updateServicesController);
router.delete("/services/:id", deleteServiceController);
router.put("/rooms/:id", changeRoomsController);
router.post("/:id", getOneBillsController);

module.exports = router;
