const router = require("express").Router();
const validateUser = require("../../../middlewares/validateUser");
const validateTakeShiftUsers = require("../../../middlewares/validateTakeShiftUsers");
const refresh = require("../../../controller/users/tokens/refresh");

router.get("/refresh", refresh);
router.use(validateUser);
router.use("/info", require("./info"));
router.use(validateTakeShiftUsers);
router.use("/bills", require("./bills"));
router.use("/rooms", require("./rooms"));
router.use("/services", require("./services"));
router.use("/shifts", require("./shifts"));

module.exports = router;
