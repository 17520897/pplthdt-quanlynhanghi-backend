const router = require("express").Router();

const getEmployeesInfoController = require("../../../../controller/users/info/getEmployeesInfoController");
const updateEmployeesInfoController = require("../../../../controller/users/info/updateEmployeesInfoController");

router.get("/", getEmployeesInfoController);
router.put("/", updateEmployeesInfoController);

module.exports = router;
