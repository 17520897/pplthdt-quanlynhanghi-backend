const router = require("express").Router();
const getAllServicesController = require("../../../../controller/users/services/getAllServicesController");

router.get("/", getAllServicesController);

module.exports = router;
