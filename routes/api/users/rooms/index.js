const getAllRoomsController = require("../../../../controller/users/rooms/getAllRoomsController");
const getUsedRoomsController = require("../../../../controller/users/rooms/getUsedRoomsController");
const getUnusedAndFixedRoomsController = require("../../../../controller/users/rooms/getUnusedAndFixedRoomsController");

const router = require("express").Router();

router.get("/", getAllRoomsController);
router.get("/used", getUsedRoomsController);
router.get("/unused", getUnusedAndFixedRoomsController);

module.exports = router;
