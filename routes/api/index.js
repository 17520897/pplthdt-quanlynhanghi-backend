const router = require("express").Router();

router.use("/admin", require("./admin"));
router.use("/auth", require("./auth"));
router.use("/users", require("./users"));

module.exports = router;
