var nodemailer = require("nodemailer");

const sendMail = async (toEmail, subject, html) => {
  try {
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      SMTPAuth: true,
      SMTPSecure: "tls",
      auth: {
        user: "uit.tutors.website@gmail.com",
        pass: "uittuttors2609"
      }
    });
    var mailOptions = {
      from: "uit.tutors@gmail.com",
      to: toEmail,
      subject,
      html
    };

    let info = await transporter.sendMail(mailOptions);
    if (info) {
      return {
        success: true,
        payload: info
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message
    };
  }
};

module.exports = {
  sendMail
};
