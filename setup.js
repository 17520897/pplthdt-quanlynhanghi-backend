require("dotenv").config();
require("./model/connect");
require("./model/schema");
const mongoose = require("mongoose");
const createUser = require("./functions/users/createUsers");

const setup = async () => {
  await mongoose.model("users").findOneAndDelete({ username: "admin" });
  const { success, payload, message } = await createUser({
    password: "admin",
    username: "admin",
    role: "admin"
  });
  await mongoose.model("params").deleteMany();
  let params = await mongoose.model("params").create({});
  if (params) {
    console.log("Tạo params thành công");
  } else {
    console.log("Tạo params thất bại");
  }
  if (success) {
    console.log(
      "Tạo tài khoản admin thành công\nTài khoản: admin, Mật khẩu: admin"
    );
  } else {
    console.log(message);
  }
};

setup();
