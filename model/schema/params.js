var mongoose = require("mongoose");

var params = new mongoose.Schema({
  pricePerHourCommonDayCommonRoom: {
    type: Number,
    default: 0
  },
  pricePer12HourCommonDayCommonRoom: {
    type: Number,
    default: 0
  },
  pricePerHourCommonDayColdRoom: {
    type: Number,
    default: 0
  },
  pricePer12HourCommonDayColdRoom: {
    type: Number,
    default: 0
  },
  pricePerHourHolidayCommonRoom: {
    type: Number,
    default: 0
  },
  pricePer12HourHolidayCommonRoom: {
    type: Number,
    default: 0
  },
  pricePerHourHolidayColdRoom: {
    type: Number,
    default: 0
  },
  pricePer12HourHolidayColdRoom: {
    type: Number,
    default: 0
  },
  setTodayIsHoliday: {
    type: Boolean,
    default: false
  },
  setWeekendIsHoliday: {
    type: Boolean,
    default: true
  },
  extraTime: {
    type: Number,
    default: 0
  }
});

module.exports = params;
