var mongoose = require("mongoose");

var shifts = new mongoose.Schema({
  tranferShiftId: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true
  },
  tranferShiftName: {
    type: String,
    required: true
  },
  takeShiftId: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true
  },
  takeShiftName: {
    type: String,
    required: true
  },
  currentMoney: {
    type: Number,
    required: true
  },
  timeTranferShift: {
    type: Date,
    required: true
  },
  shiftsOfTakeShiftEmployees: {
    day: {
      type: String
    },
    dayOfWeeks: {
      type: String,
      enum: ["0", "1", "2", "3", "4", "5", "6"]
    },
    startTime: {
      type: String
    },
    endTime: {
      type: String
    }
  },
  note: {
    type: String
  }
});

module.exports = shifts;
