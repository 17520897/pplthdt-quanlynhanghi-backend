var mongoose = require("mongoose");

var services = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    require: true
  }
});

module.exports = services;
