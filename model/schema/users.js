var mongoose = require("mongoose");

var users = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ["user", "admin"],
    require: true
  }
});

module.exports = users;
