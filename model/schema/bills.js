var mongoose = require("mongoose");

var bills = new mongoose.Schema({
  roomId: {
    type: mongoose.SchemaTypes.ObjectId
  },
  employeesId: {
    type: mongoose.SchemaTypes.ObjectId
  },
  employeesName: {
    type: String,
    required: true
  },
  roomType: {
    type: String,
    required: true
  },
  roomName: {
    type: String,
    required: true
  },
  rentType: {
    type: String,
    required: true,
    enum: ["12Hour", "perHour"]
  },
  pricePerHour: {
    type: Number,
    required: true
  },
  pricePer12Hour: {
    type: Number,
    required: true
  },
  rentHour: {
    type: Number
  },
  checkinDate: {
    type: Date
  },
  checkoutDate: {
    type: Date
  },
  services: [
    {
      serviceName: {
        type: String
      },
      quantity: {
        type: Number
      },
      price: {
        type: Number
      }
    }
  ],
  customerName: {
    type: String,
    required: true
  },
  customerIdentity: {
    type: String,
    required: true
  },
  totalPrice: {
    type: Number,
    required: true,
    default: 0
  }
});

module.exports = bills;
