var mongoose = require("mongoose");

var rooms = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true,
    enum: ["common", "cold"]
  },
  status: {
    type: String,
    enum: ["used", "unused", "fixed"],
    require: true
  }
});

module.exports = rooms;
