var mongoose = require("mongoose");

var incomes = new mongoose.Schema({
  totalMoney: {
    type: Number,
    required: true
  },
  takeMoney: {
    type: Number,
    required: true
  },
  currentMoney: {
    type: Number,
    required: true
  },
  time: {
    type: Date,
    required: true
  },
  employeesInShift: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true
  },
  nameEmployeesInShift: {
    type: String
  },
  note: {
    type: String
  }
});

module.exports = incomes;
