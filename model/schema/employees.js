var mongoose = require("mongoose");

var employees = new mongoose.Schema({
  name: {
    type: String
  },
  identity: {
    type: String
  },
  address: {
    type: String
  },
  shifts: [
    {
      day: {
        type: String
      },
      dayOfWeeks: {
        type: String,
        enum: ["0", "1", "2", "3", "4", "5", "6"]
      },
      startTime: {
        type: String
      },
      endTime: {
        type: String
      }
    }
  ],
  salary: {
    type: Number
  },
  accountId: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "users"
  },
  isInShift: {
    type: Boolean,
    default: false
  }
});

module.exports = employees;
