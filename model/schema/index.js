module.exports = {
  users: require("./users"),
  rooms: require("./rooms"),
  services: require("./services"),
  params: require("./params"),
  employees: require("./employees"),
  bills: require("./bills"),
  shifts: require("./shifts"),
  incomes: require("./incomes")
};
