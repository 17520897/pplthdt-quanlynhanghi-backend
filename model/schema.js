var mongoose = require("mongoose");
var schema = require("./schema/index");

module.exports = {
  rooms: mongoose.model("rooms", schema.rooms),
  users: mongoose.model("users", schema.users),
  services: mongoose.model("services", schema.services),
  params: mongoose.model("params", schema.params),
  employees: mongoose.model("employees", schema.employees),
  bills: mongoose.model("bills", schema.bills),
  shifts: mongoose.model("shifts", schema.shifts),
  incomes: mongoose.model("incomes", schema.incomes)
};
