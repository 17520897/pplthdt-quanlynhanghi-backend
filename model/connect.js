var mongoose = require("mongoose");
mongoose.connect(`mongodb://localhost:27017/${process.env.DB_COLLECTION}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});
